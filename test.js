var tcp = require('net');
var expect = require("chai").expect;
var assert = require("chai").assert;
var format = require('util').format;
var sleep = require('sleep');


var check_limit =  false;
var reached_limit = false;

//var port = 20102;
var port = 6667;
//var ip = "130.239.18.119";
var ip = "127.0.0.1";

var RfcSession = function() {
	var clients = [];
};



RfcSession.prototype.removeClient = function (client) {

	it("Remove connection:"+client.idx, function() {
		client.socket.destroy();

	});

}

RfcSession.prototype.execute_command = function(command, delay, clients, client_idx, opts) {

	var end = '\r\n';

	var message = command + end;
	
	for(var i = 0; i < clients.length; i++) {
		clients[i].msg_recvd = false;
		clients[i].msg = "";
	}
	
	var byte_stream = false;
	if(typeof opts !== "undefined") {
		if (opts["byte_stream"])
			byte_stream = true;
	}
	if(byte_stream) {

		for(var i = 0; i < message.length; i++) {
			var res = message.charAt(i);
			clients[client_idx - 1].socket.write(res);
		}

	} else{
		clients[client_idx - 1].socket.write(message);
	}
	sleep.sleep(delay);

}

RfcSession.prototype.connect = function (host, port, idx) {

	rfc_session = this;

	var client = {};
	client.socket = new tcp.Socket();
	client.connected = false;
	client.host = host;
	client.port = port;
	client.idx =idx;
	client.msg_recvd = false;
	client.done_called = false;
	client.msg = "";
	


	it("new connection:"+idx, function(done) {

		client.socket.on('data', function(data) {

			var string = format("client%d:\n%s",client.idx,data);
			console.log(string);
			client.msg_recvd = true;
			client.msg = data;
			
		});

		if (reached_limit == false) {
			client.socket.connect(client.port, client.host, function() {

				console.log("connected" + client.idx);
				client.connected = true;		
				client.done_called = true;
				done();

			});
		} else {
			done();
		}
	
		client.socket.on('close', function() {

			console.log("closing connection", client.idx);
				client.connected = false;

		});
		
		client.socket.on('error', function(err){
			console.log("error connected "+ client.idx);
			wait = 0;
			if(check_limit) {
				reached_limit = true;
			} else {
				expect(true).to.not.be.ok;
			}

			if(!client.done_called)
				done();

		});
	});


	client.connection_established = function (expected) {
		var client = this;
		it(format('%d\t-> connection', client.idx), function() {
			expect(true).to.equal(client.connected);
		});
	};

	return client;

};



describe('Testing IRC RFC Protocol', function () {
	

	
	var test_case = 1;
	this.timeout(100000);
	var rfc_sessions = new RfcSession();
	
	var client1 = rfc_sessions.connect(ip, port, 1);
	var client2 = rfc_sessions.connect(ip, port, 2);
	var client3 = rfc_sessions.connect(ip, port, 3);
	var client4 = rfc_sessions.connect(ip, port, 4);
	var client5 = rfc_sessions.connect(ip, port, 5);
	var client6 = rfc_sessions.connect(ip, port, 6);

	var client_list = [client1, client2, client3, client4, client5, client6];

	it("Client1: NICK", function(done){
		assert.isOk(false,"trigerred segmentation fault"); done();return;
		RfcSession.prototype.execute_command("NICK", 1, client_list, 1);
		
		done();
	});

	it("client1: Verify message", function(done){
		assert.isOk(false,"trigerred segmentation fault"); done();return;
		var patt = new RegExp(":.+ 431 .*:No nickname given");
		var res = patt.test(client1.msg);
 		
 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 431 .*:No nickname given" +
			"\n\tResponse received:" +
			"\n\t" + client1.msg);
		//assert.isOk(res, "Expected format exptec\n\t ":.+ 431 .*:No nickname given");
		done();
				

	});


	it("Client1: NICK nick1", function(done){
		
		RfcSession.prototype.execute_command("NICK nick1", 1, client_list, 1);
		
		done();
	});



	it("No response is expected from the server", function(done){

			var response = client1.msg_recvd == false && 
				client2.msg_recvd == false && client3.msg_recvd == false && 
				client4.msg_recvd == false && client5.msg_recvd == false;
		
			expect(response).equals(true);
			done();	

	});

	it("Client2: NICK nick1 (Testing  nickname in use)", function(done){

		RfcSession.prototype.execute_command("NICK nick1", 1, client_list, 2);
	
		done();
	});

	it("client2: Verify message", function(done){

		var patt = new RegExp(":.+ 433 .*nick1 :Nickname is already in use");
		var res = patt.test(client2.msg);
 
		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 433 .*nick1 :Nickname is already in use" +
			"\n\tResponse received:" +
			"\n\t" + client2.msg);

		done();
				

	});


	it("Client2: NICK 12ni", function(done){

		RfcSession.prototype.execute_command("NICK 12ni", 1, client_list, 2);
	
		done();
	});

	it("client2: Verify message", function(done){

		var patt = new RegExp(":.+ 432.*12ni :Erroneus nickname");
		var res = patt.test(client2.msg);
 		
 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 432.*12ni :Erroneus nickname" +
			"\n\tResponse received:" +
			"\n\t" + client2.msg);

		//expect(res).equals(true);
		done();
				

	});	

	it("Client2: NICK NICK1 (Testing  case-insensitive)", function(done){

		RfcSession.prototype.execute_command("NICK NICK1", 1, client_list, 2);
	
		done();
	});

	it("client2: Verify message", function(done){

		var patt = new RegExp(":.+ 433 .*NICK1 :Nickname is already in use");
		var res = patt.test(client2.msg);

 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 433 .*NICK1 :Nickname is already in use" +
			"\n\tResponse received:" +
			"\n\t" + client2.msg); 
		
		done();
				

	});	

	it("Client2: NICK nick2", function(done){

		RfcSession.prototype.execute_command("NICK nick2", 1, client_list, 2);
	
		done();
	});

	it("Client2: JOIN #chan", function(done){

		RfcSession.prototype.execute_command("JOIN #chan", 1, client_list, 2);
		done();
	});


	it("client2: Verify message", function(done){

		var patt = new RegExp(":.+ 451 nick2 :You have not registered");
		var res = patt.test(client2.msg);
 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 451 nick2 :You have not registered" +
			"\n\tResponse received:" +
			"\n\t" + client2.msg);  
		expect(res).equals(true);
		done();
				

	});	


	it("Client3: NICK nick3", function(done){
		RfcSession.prototype.execute_command("NICK nick3", 1, client_list, 3);
		sleep.sleep(1);
		done();
	});

	it("Client1: USER", function(done){

		RfcSession.prototype.execute_command("USER", 1, client_list, 1);
		
		done();
	});


	it("Client1: Verify message", function(done){

		var patt = new RegExp(":.+ 461 nick1 USER :Not enough parameters");
		var res = patt.test(client1.msg);
 
		expect(res).equals(true);
		done();
				

	});	



	it("Client1:USER user1 1 1 :realuser1", function(done){

		RfcSession.prototype.execute_command("USER user1 1 1 :realuser1" , 1, client_list, 1);
		done();
	});



	it("Client1: USER user1 1 1 :realuser1 (Trying to register again)", function(done){

		RfcSession.prototype.execute_command("USER user1 1 1 :realuser1" , 1, client_list, 1);
		done();
	});


	it("Client1: Verify message", function(done){

		var patt = new RegExp(":.+ 462 nick1 :You may not reregister");
		var res = patt.test(client1.msg);

 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 462 nick1 :You may not reregister" +
			"\n\tResponse received:" +
			"\n\t" + client1.msg);  
 
		expect(res).equals(true);
		done();
				

	});	

	it("Client2: USER user2 1 1 :realuser2", function(done){

		RfcSession.prototype.execute_command("USER user2 1 1 :realuser2", 1, client_list, 2);
		done();
	});

	it("Client3: USER user3 1 1 :realuser3", function(done){
		RfcSession.prototype.execute_command("USER user3 1 1 :realuser3", 1, client_list, 3);
		done();
	});



	it("Client4: USER user4 1 1 1 :realuser4", function(done){
		RfcSession.prototype.execute_command("USER user4 1 1 1 :realuser4", 1, client_list, 4);
	
		done();
	});

	it("Client4: NICK nick4", function(done){
		//assert.isOk(false,"trigerred segmentation fault"); done();return;
		RfcSession.prototype.execute_command("NICK nick4", 1, client_list, 4);

		done();
	});





	it("Client1: JOIN", function(done){
		//assert.isOk(false,"trigerred segmentation fault"); done();return;
		RfcSession.prototype.execute_command("JOIN", 1, client_list, 1);
		
		done();
	});

	it("Client1: Verify message", function(done){

		var patt = new RegExp(":.+ 461 nick1 JOIN :Not enough parameters");
		var res = patt.test(client1.msg);

 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 461 nick1 JOIN :Not enough parameters" +
			"\n\tResponse received:" +
			"\n\t" + client1.msg);  

		done();
				

	});	

	it("Client1:JOIN nochan", function(done){
		//assert.isOk(false,"trigerred segmentation fault"); done();return;
		RfcSession.prototype.execute_command("JOIN nochan", 1, client_list, 1);
		done();
	});

	it("Client1: Verify message", function(done){
		//assert.isOk(false,"trigerred segmentation fault"); done();return;
		var patt = new RegExp(":.+ 475 nick1 nochan :Cannot join channel");
		var res = patt.test(client1.msg);
 
 		assert.isOk(res, "\n\tExpected format(regular expression):" + 
			"\n\t:.+ 475 nick1 nochan :Cannot join channel" +
			"\n\tResponse received:" +
			"\n\t" + client1.msg); 

		done();
				

	});	


	it("Client1:JOIN #chan", function(done){

		RfcSession.prototype.execute_command("JOIN #chan", 1, client_list, 1);
		done();
	});

	it("Should receive one client response from server", function(done){

			var response = client1.msg_recvd == true && 
				client2.msg_recvd == false && client3.msg_recvd == false && 
				client4.msg_recvd == false && client5.msg_recvd == false;
			
			//console.log(client1.idx, client1.msg_recvd);
			expect(response).equals(true);
			done();
			

	});

	it("Client2: JOIN #chan", function(done){

		RfcSession.prototype.execute_command("JOIN #chan", 1, client_list, 2);
		done();
	});


	// it("Verify message format\n\t" + 
	// 	"client1 shoud receive the message of the format:\n\t" +
	// 		"353 <=|@> <channel> :[[@|+]<nick> [[@|+]<nick> [...]]]\n\t" +
	// 		"363 <channel> :End of /NAMES list\n\t"+
	it("client1: Verify message format from server received", function(done){

		var patt1 = new RegExp(":.+ 353 nick2 = #chan :nick1 nick2");
		var patt2 = new RegExp(":nick2!user2@.+ JOIN #chan");
		var res = patt1.test(client2.msg) &&
			patt2.test(client1.msg) && patt2.test(client2.msg);
 
		expect(res).equals(true);
		done();
				

	});


	it("Client3: JOIN #chan", function(done){
		RfcSession.prototype.execute_command("JOIN #chan", 1, client_list, 3);
		done();
	});

	it("Should receive three client response from server", function(done){

		var response = client1.msg_recvd == true && 
			client2.msg_recvd == true && client3.msg_recvd == true && 
			client4.msg_recvd == false && client5.msg_recvd == false;

		expect(response).equals(true);
		done();
		

	});



	it("Client1: LIST", function(done){

		RfcSession.prototype.execute_command("LIST", 1, client_list, 1);
		done();
	});

	it("Client1: LIST: Verify message from server", function(done){

		var patt1 = new RegExp(":.+ 321 nick1 Channel :Users Name");
		var patt2 = new RegExp(":.+ 322 nick1 #chan 3 ");
		var patt3 = new RegExp(":.+ 323 nick1 :End of /LIST");
		
		var res = patt1.test(client1.msg) && patt2.test(client1.msg)
			&& patt1.test(client1.msg);
 
		expect(res).equals(true);
		done();

	});


	it("Client1: NICK nick11", function(done){

		RfcSession.prototype.execute_command("NICK nick11", 1, client_list, 1);
		done();
	});

	it("Should receive three client response from server", function(done){

		var response = client1.msg_recvd == true && 
				client2.msg_recvd == true && client3.msg_recvd == true && 
				client4.msg_recvd == false && client5.msg_recvd == false;

		expect(response).equals(true);
		done();
	});


	it("Nick change : Verify message broadcasted to all clients", function(done){

		var patt = new RegExp(":nick1!user1@.+ NICK nick11");
		var res = patt.test(client1.msg) && patt.test(client2.msg)
			&& patt.test(client2.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Client5: NICK nick5", function(done){
		RfcSession.prototype.execute_command("NICK nick5", 1, client_list, 5);
		done();
	});

	it("Client5: USER user5 1 1 1 :realuser5", function(done){
		RfcSession.prototype.execute_command("USER user5 1 1 1 :realuser5", 1, client_list, 5);
		
		done();
	});


	it("Client5: JOIN #chan", function(done){
		RfcSession.prototype.execute_command("JOIN #chan", 1, client_list, 5);
		
		done();
	});

	it("Client5: PART", function(done){
		RfcSession.prototype.execute_command("PART", 1, client_list, 5);
		
		done();
	});

	it("Clinet5: PART: Verify message", function(done){

		var patt = new RegExp(":.+ 461 nick5 PART :Not enough parameters");
		var res = patt.test(client5.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Client5: PART #nyu", function(done){
		RfcSession.prototype.execute_command("PART #nyu", 1, client_list, 5);
		
		done();
	});

	it("Clinet5: PART: Verify message", function(done){

		var patt = new RegExp(":.+ 461 nick5 PART :Not enough parameters");
		var res = patt.test(client5.msg);
 
		expect(res).equals(true);
		done();
				

	});

	
	it("Client6: PART #nyu", function(done){
		RfcSession.prototype.execute_command("PART #nyu", 1, client_list, 6);
		
		done();
	});


	it("Clinet6: PART #nyu: Verify message", function(done){

		var patt = new RegExp(":.+ 451 .* :You have not registered");
		var res = patt.test(client6.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Client4: PART #chan", function(done){
		RfcSession.prototype.execute_command("PART #chan", 1, client_list, 4);
		
		done();
	});

	it("Clinet4: PART #chan: Verify message", function(done){

		var patt = new RegExp(":.+ 442 nick4 #chan :You're not on that channel");
		var res = patt.test(client4.msg);
 
		expect(res).equals(true);
		done();
				

	});


	it("Client5: PART #chan", function(done){
		RfcSession.prototype.execute_command("PART #chan", 1, client_list, 5);
		
		done();
	});

	it("Client5: PART: Verify message broadcasted to all clients", function(done){

		var patt = new RegExp(":nick5!user5@.+ (PART|QUIT) ");
		var res = patt.test(client1.msg) && patt.test(client2.msg)
			&& patt.test(client3.msg) && patt.test(client5.msg);
 
		expect(res).equals(true);
		done();
				
	});


	it("Client1: PRIVMSG ", function(done){

		RfcSession.prototype.execute_command("PRIVMSG ", 1, client_list, 1);
		done();
	});

	it("Client1: PRIVMSG: verify message", function(done){

		var patt = new RegExp(":.+ 411 nick11 :No recipient given (PRIVMSG)");
		var res = patt.test(client1.msg);
 
		expect(res).equals(true);
		done();
				

	});


	it("Client1: PRIVMSG #nochan :Hey there you", function(done){

		RfcSession.prototype.execute_command("PRIVMSG #nochan :Hey there you", 1, client_list, 1);
		done();
	});

	it("Client1: PRIVMSG: verify message", function(done){

		var patt = new RegExp(":.+ 401 nick11 #nochan :No such nick/channel");
		var res = patt.test(client1.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Client1: PRIVMSG nonickname :Hey there you", function(done){

		RfcSession.prototype.execute_command("PRIVMSG nonickname :Hey there you", 1, client_list, 1);
		done();
	});

	it("Client1: PRIVMSG nonickname: verify message", function(done){

		var patt = new RegExp(":.+ 401 nick11 nonickname :No such nick/channel");
		var res = patt.test(client1.msg);
 
		expect(res).equals(true);
		done();
				

	});


	it("Client1: PRIVMSG nick2 ", function(done){

		RfcSession.prototype.execute_command("PRIVMSG nick2 ", 1, client_list, 1);
		done();
	});

	it("Client1: PRIVMSG nick2: verify  message", function(done){

		var patt = new RegExp(":.+ 412 nick11 :No text to send");
		var res = patt.test(client1.msg);
 
		expect(res).equals(true);
		done();
	});



	it("Client1: PRIVMSG #chan :Hey there you", function(done){

		RfcSession.prototype.execute_command("PRIVMSG #chan :Hey there you", 1, client_list, 1);
		done();
	});

	it("Should receive two client response from server", function(done){

		var response = client1.msg_recvd == false && 
				client2.msg_recvd == true && client3.msg_recvd == true && 
				client4.msg_recvd == false && client5.msg_recvd == false;

		expect(response).equals(true);
		done();
		

	});

	it("Client1: PRIVMSG #chan: Verify message broadcasted to all clients", function(done){

		var patt = new RegExp(":nick11!user1@.+ PRIVMSG #chan :Hey there you");
		var res = patt.test(client2.msg) && patt.test(client3.msg);
 
		expect(res).equals(true);
		done();
				

	});



	it("Client5: WHO #chan", function(done){
		RfcSession.prototype.execute_command("WHO #chan", 1, client_list, 5);
		
		done();
	});

	it("Client5: WHO #chan: Verify message", function(done){

		var patt1 = new RegExp(":.+ 352 nick5 #chan user1 .+ .+ nick11 . :. realuser1");
		var patt2 = new RegExp(":.+ 352 nick5 #chan user2 .+ .+ nick2 . :. realuser2");
		var patt3 = new RegExp(":.+ 352 nick5 #chan user3 .+ .+ nick3 . :. realuser3");
		var patt4 = new RegExp(":.+ 315 nick5 #chan :End of /WHO list");
		var res = patt1.test(client5.msg) && patt2.test(client5.msg)
			&& patt3.test(client5.msg) && patt4.test(client5.msg);
 
		expect(res).equals(true);
		done();
				
	});

	it("Client5: WHO #chan1111", function(done){
		RfcSession.prototype.execute_command("WHO #chan1111", 1, client_list, 5);
		
		done();
	});

	it("Clinet5: WHO #chan1111: Verify message", function(done){

		
		var patt = new RegExp(":.+ 315 nick5 #chan1111 :End of /WHO list");
		var res = patt.test(client5.msg);
 
		expect(res).equals(true);
		done();
				
	});	

	it("Client4: JOIN #nyu", function(done){
		RfcSession.prototype.execute_command("JOIN #nyu", 1, client_list, 4);
		done();
	});

	it("Client4: change NiCK name nick4", function(done){
		RfcSession.prototype.execute_command("NICK nick4", 1, client_list, 4);

		done();
	});


	it("Client1: QUIT :Break time", function(done){


		RfcSession.prototype.execute_command("QUIT :Break time", 1, client_list, 1);
		done();
	});

	it("Should receive two clients response from server", function(done){

		var response = client1.msg_recvd == false && 
			client2.msg_recvd == true && 
			client3.msg_recvd == true && 
			client4.msg_recvd == false;

		expect(response).equals(true);
		done();
		

	});

	it("Client1: QUIT: Verify message broadcasted to all clients", function(done){

		var patt = new RegExp(":nick11!user1@.+ QUIT");
		var res = patt.test(client2.msg) && patt.test(client3.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Remove client2", function(done) {
		client2.socket.destroy();
		sleep.sleep(2);
		done();
	});
	it("Client2: remove client2: Verify message broadcasted to all clients", function(done){

		var patt = new RegExp(":nick2!user2@.+ QUIT");
		var res = patt.test(client3.msg);
 
		expect(res).equals(true);
		done();
				

	});

	it("Client3:LIST ", function(done){

		RfcSession.prototype.execute_command("LIST", 1, client_list, 3);
		
		done();
	});


	it("Client3: LIST: Verify message: expect #chan 1 #nyu 1", function(done){

		var patt1 = new RegExp(":.+ 321 nick3 Channel :Users Name");
		var patt2 = new RegExp(":.+ 322 nick3 #chan 1");
		var patt3 = new RegExp(":.+ 322 nick3 #nyu 1");
		var patt4 = new RegExp(":.+ 323 nick3 :End of /LIST");
		
		var res = patt1.test(client3.msg) && patt2.test(client3.msg)
			&& patt3.test(client3.msg) && patt3.test(client3.msg);
 
		expect(res).equals(true);
		done();

	});



	it("Client5: PRIVMSG nick3 :Hey nick3, how are you? ", function(done){

		RfcSession.prototype.execute_command("PRIVMSG nick3 :Hey nick3, how are you?", 1, client_list, 5);
		done();
	});



	it("Client6: NICK nick6 & USER", function(done){

		RfcSession.prototype.execute_command("NICK nick6\r\nUSER", 1, client_list, 6);
		done();
	});

	

	it("Should receive one error response from server", function(done){

		var response = client6.msg_recvd == true;

	
		expect(response).equals(true);
		done();
		

	});

	it("Client6: NICK 888nic66666666666666", function(done){

		RfcSession.prototype.execute_command("NICK 888nic66666666666666", 1, client_list, 6, {"byte_stream":true});
		done();
	});

	

	it("Should receive one error response from server", function(done){

		var response = client6.msg_recvd == true;

	
		expect(response).equals(true);
		done();
		

	});


	it("Close all reminaining connections: send QUIT", function(done){


		RfcSession.prototype.execute_command("QUIT", 1, client_list, 3);
		RfcSession.prototype.execute_command("QUIT", 1, client_list, 4);
		RfcSession.prototype.execute_command("QUIT", 1, client_list, 5);
		
		// 6 not registered. hence 'quit' command will not be processed
		// will be accepted.
		RfcSession.prototype.execute_command("QUIT", 1, client_list, 6);
		client6.socket.destroy(); 
		check_limit = true;
		done();
	});

	//Test maximum clients that a server can handle//
	var new_client_list = [];
	var client_indx ;
	
	for(client_indx=0; client_indx < 1500; client_indx++) {

		new_client_list.push(rfc_sessions.connect(ip, port, client_indx));


	}

	//If server reaches maximum clients that it can hanlde,
	//remove one client and reconnect again to verify if server does proper cleanup
	//when client closes its connection

	it("Close   connection 1: destroy client", function(done){
		sleep.sleep(3);
		new_client_list[0].socket.destroy();
		console.log(new_client_list[0].idx);
		check_limit = false;
		reached_limit = false;
		sleep.sleep(10);
		done();
	});

	
	new_client_list[client_indx] = rfc_sessions.connect(ip, port, client_indx);
	console.log(client_indx);
	client_indx++;

	it("Client1: send NiCK", function(done){
		console.log(client_indx);
		RfcSession.prototype.execute_command("NICK nick1", 1, new_client_list, client_indx);
		done();
	});

	it("Client1: send USER", function(done){
		RfcSession.prototype.execute_command("USER user1 1 1 1 :1", 1, new_client_list, client_indx);
	
		done();
	});


	it("Client1: send JOIN", function(done){
		RfcSession.prototype.execute_command("JOIN #nyu", 0, new_client_list, client_indx);
		new_client_list[client_indx-1].socket.destroy();
		sleep.sleep(5);
		done();
	});

	rfc_sessions.connect('127.0.0.1', port, client_indx+1);
	rfc_sessions.connect('127.0.0.1', port, client_indx+2);
	//client_indx++;
		
	
});


